Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0029-placer-s).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0029-placer-s/-/raw/main/01_operating_manual/S2-0029_A_Betriebsanleitung.pdf)|
|assembly drawing           |[de/en](https://gitlab.com/ourplant.net/products/s2-0029-placer-s/-/raw/main/02_assembly_drawing/s2-0029_B_ZNB_staking_head.pdf)|
|circuit diagram            |[de/en](https://gitlab.com/ourplant.net/products/s2-0029-placer-s/-/raw/main/03_circuit_diagram/S2-0029-EPLAN-A.pdf)|
|maintenance instructions   ||
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0029-placer-s/-/raw/main/05_spare_parts/S2-0029_B_EVL_Staking%20Head.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
